# labJava#

This the singular lab repository for Java Programming 51036 UChicago

### Here you will find all the labs for this course  ###

* you will fork and clone this lab, but you can always create an alternative remote to this repo by issuing this command: git remote add gerber https://csgerber@bitbucket.org/csgerber/labjava.git
* If you want to learn Markdown -> [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)